package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type OrderBookInput struct {
	StoreName			string				`bson:"store_name" json:"store_name" validate:"required"`
	BookTitle			string				`bson:"book_title" json:"book_title" validate:"required"`
}

type BookBuyerRelationship struct {
	Id					primitive.ObjectID				`bson:"_id" json:"_id"`
	BuyerId 			primitive.ObjectID				`bson:"buyer_id" json:"buyer_id"`
	BookId 				primitive.ObjectID				`bson:"book_id" json:"book_id"`
	CreatedAt			*time.Time						`bson:"created_at" json:"created_at"`
}

type BuyerBookData struct {
	BookTitle			string							`bson:"book_title" json:"book_title"`
	BookAuthor			string							`bson:"book_author" json:"book_author"`
	YearPublished		string							`bson:"year_published" json:"year_published"`
}

type BookListByBuyer struct {
	UserName			string							`bson:"user_name" json:"user_name"`
	Role				string							`bson:"role" json:"role"`
	BookList			[]BuyerBookData					`bson:"book_list" json:"book_list"`
}