package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BookListInput struct {
	BookTitle			string							`bson:"book_title" json:"book_title" validate:"required"`
	Stock				int32							`bson:"stock" json:"stock" validate:"required"`
}

type StoreInput struct {
	StoreName 			string							`bson:"store_name" json:"store_name" validate:"required"`
	Address				string							`bson:"address" json:"address" validate:"required"`
	BookList			[]BookListInput					`bson:"book_list" json:"book_list" validate:"required"`
}

type UpdateStoreInput struct {
	StoreName 			string							`bson:"store_name" json:"store_name" validate:"required"`
	Address				string							`bson:"address" json:"address" validate:"required"`
}

type UpdateBookStock struct {
	StoreName 			string							`bson:"store_name" json:"store_name" validate:"required"`
	BookTitle			string							`bson:"address" json:"address" validate:"required"`
	Stock				int32							`bson:"stock" json:"stock" validate:"required"`
}

type Store struct {
	Id        			primitive.ObjectID				`bson:"_id" json:"_id"`
	StoreName  			string							`bson:"store_name" json:"store_name"`
	Address				string							`bson:"address" json:"address"`
	CreatedAt 			*time.Time						`bson:"created_at" json:"created_at"`
	UpdatedAt			*time.Time						`bson:"updated_at" json:"updated_at"`
}