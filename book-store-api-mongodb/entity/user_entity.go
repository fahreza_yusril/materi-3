package entity

import (
	"time"

	jwt "github.com/dgrijalva/jwt-go/v4"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserRegisterInput struct {
	UserName 			string							`bson:"user_name" json:"user_name" validate:"required"`
	Password 			string							`bson:"password" json:"password" validate:"required"`
	Role				string							`bson:"role" json:"role" validate:"required"`
}

type UserInput struct {
	UserName 			string							`bson:"user_name" json:"user_name" validate:"required"`
	Password 			string							`bson:"password" json:"password" validate:"required"`
}

type UserTopUpBalanceInput struct {
	Amount				int32							`bson:"amount" json:"amount" validate:"required"`
}

type User struct {
	Id					primitive.ObjectID				`bson:"_id" json:"_id"`
	UserName			string							`bson:"user_name" json:"user_name"`
	Password			string							`bson:"password" json:"password"`
	Role				string							`bson:"role" json:"role"`
	Balance				int32							`bson:"balance" json:"balance"`
	CreatedAt			*time.Time						`bson:"created_at" json:"created_at"`
	UpdatedAt			*time.Time						`bson:"updated_at" json:"updated_at"`
}

type GetUserOutput struct {
	Id					primitive.ObjectID				`bson:"_id" json:"_id"`
	UserName			string							`bson:"user_name" json:"user_name"`
	Role				string							`bson:"role" json:"role"`
	Balance				int32							`bson:"balance" json:"balance"`
	CreatedAt			*time.Time						`bson:"created_at" json:"created_at"`
	UpdatedAt			*time.Time						`bson:"updated_at" json:"updated_at"`
}

type TokenClaims struct {
	Id					string							`bson:"_id" json:"_id"`
	jwt.StandardClaims
}

type TokenOutput struct {
	AccessToken 		string							`bson:"access_token" json:"access_token"`
	RefreshToken		string							`bson:"referesh_token" json:"refresh_token"`
}