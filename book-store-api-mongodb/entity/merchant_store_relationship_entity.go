package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type MerchantStoreRelationship struct {
	Id					primitive.ObjectID				`bson:"_id" json:"_id"`
	MerchantId 			primitive.ObjectID				`bson:"merchant_id" json:"merchant_id"`
	StoreId 			primitive.ObjectID				`bson:"store_id" json:"store_id"`
	CreatedAt			*time.Time						`bson:"created_at" json:"created_at"`
}

type MerchantStoreData struct {
	StoreName			string							`bson:"store_name" json:"store_name"`
	Address				string							`bson:"address" json:"address"`
}

type StoreByMerchant struct {
	UserName			string							`bson:"user_name" json:"user_name"`
	Role				string							`bson:"role" json:"role"`
	Store				MerchantStoreData				`bson:"store" json:"store"`
}