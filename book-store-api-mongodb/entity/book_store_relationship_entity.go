package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BookStoreRelationship struct {
	Id					primitive.ObjectID				`bson:"_id" json:"_id"`
	StoreId 			primitive.ObjectID				`bson:"store_id" json:"store_id"`
	BookId 				primitive.ObjectID				`bson:"book_id" json:"book_id"`
	Stock				int32							`bson:"stock" json:"stock"`
	CreatedAt			*time.Time						`bson:"created_at" json:"created_at"`
	UpdatedAt			*time.Time						`bson:"updated_at" json:"updated_at"`
}

type BookData struct {
	BookTitle			string							`bson:"book_title" json:"book_title"`
	BookAuthor			string							`bson:"book_author" json:"book_author"`
	YearPublished		string							`bson:"year_published" json:"year_published"`
	Price 				int32							`bson:"price" json:"price"`
	Stock				int32							`bson:"stock" json:"stock"`
}

type StoreData struct {
	StoreName 			string							`bson:"store_name" json:"store_name"`
	Address				string							`bson:"address" json:"address"`
	Stock 				int32							`bson:"stock" json:"stock"`
}

type BookListByStore struct {
	StoreName			string							`bson:"store_name" json:"store_name"`
	Address				string							`bson:"address" json:"address"`
	BookList			[]BookData						`bson:"book_list" json:"book_list"`
}

type StoreListByBook struct {
	BookTitle			string							`bson:"book_title" json:"book_title"`
	BookAuthor			string							`bson:"book_author" json:"book_author"`
	YearPublished		string							`bson:"year_published" json:"year_published"`
	Price 				int32							`bson:"price" json:"price"`
	StoreList			[]StoreData						`bson:"store_list" json:"store_list"`
}