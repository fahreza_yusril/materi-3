package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type BookInput struct {
	BookTitle				string							`bson:"book_title" json:"book_title" validate:"required"`
	BookAuthor				string							`bson:"book_author" json:"book_author" validate:"required"`
	YearPublished			string							`bson:"year_published" json:"year_published" validate:"required"`
	Price 					int32							`bson:"price" json:"price" validate:"required"`
}

type Book struct {
	Id 						primitive.ObjectID				`bson:"_id" json:"_id"`
	BookTitle				string							`bson:"book_title" json:"book_title"`
	BookAuthor				string							`bson:"book_author" json:"book_author"`
	YearPublished			string							`bson:"year_published" json:"year_published"`
	Price 					int32							`bson:"price" json:"price"`
	CreatedAt				*time.Time						`bson:"created_at" json:"created_at"`
	UpdatedAt				*time.Time						`bson:"updated_at" json:"updated_at"`
}