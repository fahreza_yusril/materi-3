package handler

import (
	"book-store-api-mongodb/entity"
	"book-store-api-mongodb/helper"
	"book-store-api-mongodb/service"
	"encoding/json"
	"errors"
	"net/http"
)

type StoreHandler struct {
	storeService *service.StoreService
}

type StoreHandlerInterface interface {
	CreateStore(w http.ResponseWriter, r *http.Request)
	UpdateStore(w http.ResponseWriter, r *http.Request)
	UpdateBookStock(w http.ResponseWriter, r *http.Request)
	DeleteStore(w http.ResponseWriter, r *http.Request)
	FindStoreByName(w http.ResponseWriter, r *http.Request)
	FindAllStore(w http.ResponseWriter, r *http.Request)
	FindStoreListByBook(w http.ResponseWriter, r *http.Request)
	FindStoreByMerchant(w http.ResponseWriter, r *http.Request)
}

func NewStoreHandler(storeService *service.StoreService) StoreHandlerInterface {
	return &StoreHandler{storeService: storeService}
}

func (sh *StoreHandler) CreateStore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	merchantId := r.Header.Get("Id")
	var input entity.StoreInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := sh.storeService.CreateStore(r.Context(), &input, merchantId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (sh *StoreHandler) UpdateStore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	merchantId := r.Header.Get("Id")
	var input entity.UpdateStoreInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := sh.storeService.UpdateStore(r.Context(), &input, merchantId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (sh *StoreHandler) UpdateBookStock(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	merchantId := r.Header.Get("Id")
	var input entity.BookListInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := sh.storeService.UpdateBookStock(r.Context(), &input, merchantId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (sh *StoreHandler) DeleteStore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	merchantId := r.Header.Get("Id")

	errOutput := sh.storeService.DeleteStore(r.Context(), merchantId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse("Delete berhasil!")
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (sh *StoreHandler) FindStoreByName(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	storeName := r.URL.Query().Get("store-name")
	if storeName == "" {
		errResponse := helper.NewErrorResponse(errors.New("Param tidak boleh kosong!"))
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	output, errOutput := sh.storeService.FindStoreByName(r.Context(), storeName)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (sh *StoreHandler) FindAllStore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	output, errOutput := sh.storeService.FindAllStore(r.Context())
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}
	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (sh *StoreHandler) FindStoreListByBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	bookTitle := r.URL.Query().Get("book-title")
	if bookTitle == "" {
		errResponse := helper.NewErrorResponse(errors.New("Param tidak boleh kosong!"))
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	output, errOutput := sh.storeService.FindStoreListByBook(r.Context(), bookTitle)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (sh *StoreHandler) FindStoreByMerchant(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	merchantId := r.Header.Get("Id")

	output, errOutput := sh.storeService.FindStoreByMerchant(r.Context(), merchantId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}