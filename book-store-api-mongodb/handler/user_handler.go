package handler

import (
	"book-store-api-mongodb/entity"
	"book-store-api-mongodb/helper"
	"book-store-api-mongodb/service"
	"encoding/json"
	"net/http"
)

type UserHandler struct {
	userService *service.UserService
}

type UserHandlerInterface interface {
	RegisterUser(w http.ResponseWriter, r *http.Request)
	LoginUser(w http.ResponseWriter, r *http.Request)
	UpdateUser(w http.ResponseWriter, r *http.Request)
	TopUpBalance(w http.ResponseWriter, r *http.Request)
	DeleteUser(w http.ResponseWriter, r *http.Request)
	FindUserById(w http.ResponseWriter, r *http.Request)
	RefreshToken(w http.ResponseWriter, r *http.Request)
}

func NewUserHandler(userService *service.UserService) UserHandlerInterface {
	return &UserHandler{userService: userService}
}

func (uh *UserHandler) RegisterUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var input entity.UserRegisterInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := uh.userService.RegisterUser(r.Context(), &input)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}
	
	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (uh *UserHandler) LoginUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var input entity.UserInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := uh.userService.LoginUser(r.Context(), &input)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (uh *UserHandler) UpdateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	userId := r.Header.Get("Id")
	var input entity.UserInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := uh.userService.UpdateUser(r.Context(), &input, userId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (uh *UserHandler) TopUpBalance(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	userId := r.Header.Get("Id")
	var input entity.UserTopUpBalanceInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := uh.userService.TopUpBalance(r.Context(), &input, userId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (uh *UserHandler) DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	userId := r.Header.Get("Id")

	errOutput := uh.userService.DeleteUser(r.Context(), userId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse("Delete berhasil!")
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (uh *UserHandler) FindUserById(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	userId := r.Header.Get("Id")

	output, errOutput := uh.userService.FindUserById(r.Context(), userId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (uh *UserHandler) RefreshToken(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	userId := r.Header.Get("Id")
	
	output, errOutput := uh.userService.RefreshToken(r.Context(), userId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}