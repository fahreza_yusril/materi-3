package handler

import (
	"book-store-api-mongodb/entity"
	"book-store-api-mongodb/helper"
	"book-store-api-mongodb/service"
	"encoding/json"
	"errors"
	"net/http"
)

type BookHandler struct {
	bookService *service.BookService
}

type BookHandlerInterface interface {
	CreateBook(w http.ResponseWriter, r *http.Request)
	UpdateBook(w http.ResponseWriter, r *http.Request)
	DeleteBook(w http.ResponseWriter, r *http.Request)
	FindBookByTitle(w http.ResponseWriter, r *http.Request)
	FindAllBook(w http.ResponseWriter, r *http.Request)
	FindBookListByStore(w http.ResponseWriter, r *http.Request)
	OrderBook(w http.ResponseWriter, r *http.Request)
	FindBookListByBuyer(w http.ResponseWriter, r *http.Request)
}

func NewBookHandler(bookService *service.BookService) BookHandlerInterface {
	return &BookHandler{bookService: bookService}
}

func (bh *BookHandler) CreateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var input entity.BookInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := bh.bookService.CreateBook(r.Context(), &input)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (bh *BookHandler) UpdateBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var input entity.BookInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	bookTitle := r.URL.Query().Get("book-title")
	if bookTitle == "" {
		errResponse := helper.NewErrorResponse(errors.New("Param tidak boleh kosong!"))
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	output, errOutput := bh.bookService.UpdateBook(r.Context(), &input, bookTitle)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (bh *BookHandler) DeleteBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	bookTitle := r.URL.Query().Get("book-title")
	if bookTitle == "" {
		errResponse := helper.NewErrorResponse(errors.New("Param tidak boleh kosong!"))
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	errOutput := bh.bookService.DeleteBook(r.Context(), bookTitle)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse("Delete berhasil!")
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (bh *BookHandler) FindBookByTitle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	bookTitle := r.URL.Query().Get("book-title")
	if bookTitle == "" {
		errResponse := helper.NewErrorResponse(errors.New("Param tidak boleh kosong!"))
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	output, errOutput := bh.bookService.FindBookByTitle(r.Context(), bookTitle)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (bh *BookHandler) FindAllBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	output, errOutput := bh.bookService.FindAllBook(r.Context())
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}
	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (bh *BookHandler) FindBookListByStore(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	storeName := r.URL.Query().Get("store-name")
	if storeName == "" {
		errResponse := helper.NewErrorResponse(errors.New("Param tidak boleh kosong!"))
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	output, errOutput := bh.bookService.FindBookListByStore(r.Context(), storeName)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (bh *BookHandler) OrderBook(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	buyerId := r.Header.Get("Id")
	var input entity.OrderBookInput
	errDecode := json.NewDecoder(r.Body).Decode(&input)
	if errDecode != nil {
		panic(errDecode)
	}

	output, errOutput := bh.bookService.OrderBook(r.Context(), &input, buyerId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}

func (bh *BookHandler) FindBookListByBuyer(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	buyerId := r.Header.Get("Id")
	
	output, errOutput := bh.bookService.FindBookListByBuyer(r.Context(), buyerId)
	if errOutput != nil {
		errResponse := helper.NewErrorResponse(errOutput)
		errEncode := json.NewEncoder(w).Encode(errResponse)
		if errEncode != nil {
			panic(errEncode)
		}
	}

	successResponse := helper.NewSuccessResponse(output)
	errEncode := json.NewEncoder(w).Encode(successResponse)
	if errEncode != nil {
		panic(errEncode)
	}
}