package main

import (
	"book-store-api-mongodb/handler"
	"book-store-api-mongodb/middleware"
	"book-store-api-mongodb/repo"
	"book-store-api-mongodb/service"
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	serverAPIOptions := options.ServerAPI(options.ServerAPIVersion1)
	clientOptions := options.Client().ApplyURI("mongodb+srv://mongodb:mongodb@cluster0.hcjgy.mongodb.net/?retryWrites=true&w=majority").SetServerAPIOptions(serverAPIOptions)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
    	log.Fatal(err)
	}

	br := repo.NewBookRepo(client).(*repo.BookRepo)
	sr := repo.NewStoreRepo(client).(*repo.StoreRepo)
	ur := repo.NewUserRepo(client).(*repo.UserRepo)
	bsrr := repo.NewBookStoreRelationshipRepo(client).(*repo.BookStoreRelationshipRepo)
	bbrr := repo.NewBookBuyerRelationshipRepo(client).(*repo.BookBuyerRelationshipRepo)
	msrr := repo.NewMerchantStoreRelationshipRepo(client).(*repo.MerchantStoreRelationshipRepo)

	bs := service.NewBookService(br, sr, ur, bsrr, bbrr, msrr, client).(*service.BookService)
	ss := service.NewStoreService(br, sr, ur, bsrr, msrr, client).(*service.StoreService)
	us := service.NewUserService(ur,bbrr, msrr, client).(*service.UserService)

	bh := handler.NewBookHandler(bs).(*handler.BookHandler)
	sh := handler.NewStoreHandler(ss).(*handler.StoreHandler)
	uh := handler.NewUserHandler(us).(*handler.UserHandler)

	router := mux.NewRouter()

	router.Path("/book").HandlerFunc(bh.CreateBook).Methods("POST")
	router.Path("/book").HandlerFunc(bh.UpdateBook).Methods("PUT")
	router.Path("/book").HandlerFunc(bh.DeleteBook).Methods("DELETE")
	router.Path("/book/by-title").HandlerFunc(bh.FindBookByTitle).Methods("GET")
	router.Path("/book").HandlerFunc(bh.FindAllBook).Methods("GET")
	router.Path("/book/by-store").HandlerFunc(bh.FindBookListByStore).Methods("GET")
	orderRouter := router.Methods(http.MethodPost).Subrouter()
	orderRouter.Path("/book/order").HandlerFunc(bh.OrderBook)
	orderRouter.Use(middleware.Authenticate)
	bookListByBuyerRouter := router.Methods(http.MethodGet).Subrouter()
	bookListByBuyerRouter.Path("/book/by-buyer").HandlerFunc(bh.FindBookListByBuyer)
	bookListByBuyerRouter.Use(middleware.Authenticate)

	createStoreRouter := router.Methods(http.MethodPost).Subrouter()
	createStoreRouter.Path("/store").HandlerFunc(sh.CreateStore)
	createStoreRouter.Use(middleware.Authenticate)
	updateStoreRouter := router.Methods(http.MethodPut).Subrouter()
	updateStoreRouter.Path("/store").HandlerFunc(sh.UpdateStore)
	updateStoreRouter.Use(middleware.Authenticate)
	updateBookStockRouter := router.Methods(http.MethodPut).Subrouter()
	updateBookStockRouter.Path("/store/update-book-stock").HandlerFunc(sh.UpdateBookStock)
	updateBookStockRouter.Use(middleware.Authenticate)
	deleteStoreRouter := router.Methods(http.MethodDelete).Subrouter()
	deleteStoreRouter.Path("/store").HandlerFunc(sh.DeleteStore)
	deleteStoreRouter.Use(middleware.Authenticate)
	router.Path("/store/by-name").HandlerFunc(sh.FindStoreByName).Methods("GET")
	router.Path("/store").HandlerFunc(sh.FindAllStore).Methods("GET")
	router.Path("/store/by-book").HandlerFunc(sh.FindStoreListByBook).Methods("GET")
	storeByMerchantRouter := router.Methods(http.MethodGet).Subrouter()
	storeByMerchantRouter.Path("/store/by-merchant").HandlerFunc(sh.FindStoreByMerchant)
	storeByMerchantRouter.Use(middleware.Authenticate)
	
	router.Path("/register").HandlerFunc(uh.RegisterUser).Methods("POST")
	router.Path("/login").HandlerFunc(uh.LoginUser).Methods("POST")
	updateUserRouter := router.Methods(http.MethodPut).Subrouter()
	updateUserRouter.Path("/user").HandlerFunc(uh.UpdateUser)
	updateUserRouter.Use(middleware.Authenticate)
	topUpRouter := router.Methods(http.MethodPut).Subrouter()
	topUpRouter.Path("/user/topup").HandlerFunc(uh.TopUpBalance)
	topUpRouter.Use(middleware.Authenticate)
	deleteUserRouter := router.Methods(http.MethodDelete).Subrouter()
	deleteUserRouter.Path("/user").HandlerFunc(uh.DeleteUser)
	deleteUserRouter.Use(middleware.Authenticate)
	userByIdRouter := router.Methods(http.MethodGet).Subrouter()
	userByIdRouter.Path("/user/by-id").HandlerFunc(uh.FindUserById)
	userByIdRouter.Use(middleware.Authenticate)
	refreshTokenRouter := router.Methods(http.MethodGet).Subrouter()
	refreshTokenRouter.Path("/refresh-token").HandlerFunc(uh.RefreshToken)
	refreshTokenRouter.Use(middleware.Authenticate)
	
	srv := &http.Server{
		Handler: router,
		Addr:    "127.0.0.1:8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	fmt.Println("Listening to Port 8000")
	log.Fatal(srv.ListenAndServe())
}