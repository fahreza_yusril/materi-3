package repo

import (
	"book-store-api-mongodb/entity"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type BookStoreRelationshipRepo struct {
	client *mongo.Client
}

type BookStoreRelationshipRepoInterface interface {
	CreateBookStoreRelationship(ctx context.Context, storeId string, bookId string, stock int32) (*entity.BookStoreRelationship, error)
	UpdateBookStock(ctx context.Context, storeId string, bookId string, stock int32) (*entity.BookStoreRelationship, error)
	DeleteBookStoreRelationship(ctx context.Context, storeOrBookId string) error
	FindRelationshipByStoreAndBookId(ctx context.Context, storeId string, bookId string) (*entity.BookStoreRelationship, error)
	FindBookListByStoreId(ctx context.Context, storeId string) ([]*entity.BookStoreRelationship, error)
	FindStoreListByBookId(ctx context.Context, bookId string) ([]*entity.BookStoreRelationship, error)
}

func NewBookStoreRelationshipRepo(client *mongo.Client) BookStoreRelationshipRepoInterface {
	return &BookStoreRelationshipRepo{client: client}
}

func (bsrr *BookStoreRelationshipRepo) CreateBookStoreRelationship(ctx context.Context, storeId string, bookId string, stock int32) (*entity.BookStoreRelationship, error) {
	storeObjId, errStoreObjectIDFromHex := primitive.ObjectIDFromHex(storeId)
	if errStoreObjectIDFromHex != nil {
		return nil, errStoreObjectIDFromHex
	}
	
	bookObjId, errBookObjectIDFromHex := primitive.ObjectIDFromHex(bookId)
	if errBookObjectIDFromHex != nil {
		return nil, errBookObjectIDFromHex
	}
	
	timeStamp := time.Now().Local()
	coll := bsrr.client.Database("book_store_api").Collection("book_store_relationships")
	data := bson.D{
			{Key: "store_id", Value: storeObjId},
			{Key: "book_id", Value: bookObjId},
			{Key: "stock", Value: stock},
			{Key: "created_at", Value: timeStamp},
		}

	insert, errInsert := coll.InsertOne(ctx, data)
	if errInsert != nil {
		return nil, errInsert
	}

	output := entity.BookStoreRelationship{
		Id: insert.InsertedID.(primitive.ObjectID),
		StoreId: storeObjId,
		BookId: bookObjId,
		Stock: stock,
		CreatedAt: &timeStamp,
		UpdatedAt: nil,
	}

	return &output, nil
}

func (bsrr *BookStoreRelationshipRepo) UpdateBookStock(ctx context.Context, storeId string, bookId string, stock int32) (*entity.BookStoreRelationship, error) {
	storeObjId, errStoreObjectIDFromHex := primitive.ObjectIDFromHex(storeId)
	if errStoreObjectIDFromHex != nil {
		return nil, errStoreObjectIDFromHex
	}
	
	bookObjId, errBookObjectIDFromHex := primitive.ObjectIDFromHex(bookId)
	if errBookObjectIDFromHex != nil {
		return nil, errBookObjectIDFromHex
	}
	
	timeStamp := time.Now().Local()
	coll := bsrr.client.Database("book_store_api").Collection("book_store_relationships")
	filter := bson.D{{Key: "$and", Value: bson.A{bson.D{{Key: "store_id", Value: storeObjId}}, bson.D{{Key: "book_id", Value: bookObjId}}}}}
	data := bson.D{{
		Key: "$set", 
		Value: bson.D{
			{Key: "stock", Value: stock},
			{Key: "updated_at", Value: timeStamp},
		},
	}}

	_, errUpdate := coll.UpdateOne(ctx, filter, data)
	if errUpdate != nil {
		return nil, errUpdate
	}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.BookStoreRelationship
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (bsrr *BookStoreRelationshipRepo) DeleteBookStoreRelationship(ctx context.Context, storeOrBookId string) error {
	storeOrBookObjId, errStoreOrBookObjectIDFromHex := primitive.ObjectIDFromHex(storeOrBookId)
	if errStoreOrBookObjectIDFromHex != nil {
		return errStoreOrBookObjectIDFromHex
	}
	
	coll := bsrr.client.Database("book_store_api").Collection("book_store_relationships")
	filter := bson.D{{Key: "$or", Value: bson.A{bson.D{{Key: "store_id", Value: storeOrBookObjId}}, bson.D{{Key: "book_id", Value: storeOrBookObjId}}}}}

	_, errDelete := coll.DeleteOne(ctx, filter)
	if errDelete != nil {
		return errDelete
	}

	return nil
}

func (bsrr *BookStoreRelationshipRepo) FindRelationshipByStoreAndBookId(ctx context.Context, storeId string, bookId string) (*entity.BookStoreRelationship, error) {
	storeObjId, errStoreObjectIDFromHex := primitive.ObjectIDFromHex(storeId)
	if errStoreObjectIDFromHex != nil {
		return nil, errStoreObjectIDFromHex
	}
	
	bookObjId, errBookObjectIDFromHex := primitive.ObjectIDFromHex(bookId)
	if errBookObjectIDFromHex != nil {
		return nil, errBookObjectIDFromHex
	}
	
	coll := bsrr.client.Database("book_store_api").Collection("book_store_relationships")
	filter := bson.D{{Key: "$and", Value: bson.A{bson.D{{Key: "store_id", Value: storeObjId}}, bson.D{{Key: "book_id", Value: bookObjId}}}}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.BookStoreRelationship
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (bsrr *BookStoreRelationshipRepo) FindBookListByStoreId(ctx context.Context, storeId string) ([]*entity.BookStoreRelationship, error) {
	storeObjId, errStoreObjectIDFromHex := primitive.ObjectIDFromHex(storeId)
	if errStoreObjectIDFromHex != nil {
		return nil, errStoreObjectIDFromHex
	}
	
	coll := bsrr.client.Database("book_store_api").Collection("book_store_relationships")
	filter := bson.D{{Key: "store_id", Value: storeObjId}}

	cursor, errFind := coll.Find(ctx, filter)
	if errFind != nil {
		return nil, errFind
	}
	defer cursor.Close(ctx)

	var results []bson.D
	errResults := cursor.All(ctx, &results)
	if errResults != nil {
		return nil, errResults
	}

	outputs := make([]*entity.BookStoreRelationship, 0)
	for _, result := range results {
		doc, errMarshal := bson.Marshal(result)
		if errMarshal != nil {
			return nil, errMarshal
		}

		var output entity.BookStoreRelationship
		errUnmarshal := bson.Unmarshal(doc, &output)
		if errUnmarshal != nil {
			return nil, errUnmarshal
		}

		outputs = append(outputs, &output)
	}

	return outputs, nil
}

func (bsrr *BookStoreRelationshipRepo) FindStoreListByBookId(ctx context.Context, bookId string) ([]*entity.BookStoreRelationship, error) {
	bookObjId, errBookObjectIDFromHex := primitive.ObjectIDFromHex(bookId)
	if errBookObjectIDFromHex != nil {
		return nil, errBookObjectIDFromHex
	}
	
	coll := bsrr.client.Database("book_store_api").Collection("book_store_relationships")
	filter := bson.D{{Key: "book_id", Value: bookObjId}}

	cursor, errFind := coll.Find(ctx, filter)
	if errFind != nil {
		return nil, errFind
	}
	defer cursor.Close(ctx)

	var results []bson.D
	errResults := cursor.All(ctx, &results)
	if errResults != nil {
		return nil, errResults
	}

	outputs := make([]*entity.BookStoreRelationship, 0)
	for _, result := range results {
		doc, errMarshal := bson.Marshal(result)
		if errMarshal != nil {
			return nil, errMarshal
		}

		var output entity.BookStoreRelationship
		errUnmarshal := bson.Unmarshal(doc, &output)
		if errUnmarshal != nil {
			return nil, errUnmarshal
		}

		outputs = append(outputs, &output)
	}

	return outputs, nil
}