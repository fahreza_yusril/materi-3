package repo

import (
	"book-store-api-mongodb/entity"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type StoreRepo struct {
	client *mongo.Client
}

type StoreRepoInterface interface {
	CreateStore(ctx context.Context, input *entity.StoreInput) (*entity.Store, error)
	UpdateStore(ctx context.Context, input *entity.UpdateStoreInput, storeName string) (*entity.Store, error)
	DeleteStore(ctx context.Context, storeName string) error
	FindStoreById(ctx context.Context, storeId string) (*entity.Store, error)
	FindStoreByName(ctx context.Context, storeName string) (*entity.Store, error)
	FindAllStore(ctx context.Context) ([]*entity.Store, error)
}

func NewStoreRepo(client *mongo.Client) StoreRepoInterface {
	return &StoreRepo{client: client}
}

func (sr *StoreRepo) CreateStore(ctx context.Context, input *entity.StoreInput) (*entity.Store, error) {
	timeStamp := time.Now().Local()
	coll := sr.client.Database("book_store_api").Collection("stores")
	data := bson.D{
			{Key: "store_name", Value: input.StoreName},
			{Key: "address", Value: input.Address},
			{Key: "created_at", Value: timeStamp},
		}

	insert, errInsert := coll.InsertOne(ctx, data)
	if errInsert != nil {
		return nil, errInsert
	}

	output := entity.Store{
		Id: insert.InsertedID.(primitive.ObjectID),
		StoreName: input.StoreName,
		Address: input.Address,
		CreatedAt: &timeStamp,
		UpdatedAt: nil,
	}

	return &output, nil
}

func (sr *StoreRepo) UpdateStore(ctx context.Context, input *entity.UpdateStoreInput, storeName string) (*entity.Store, error) {
	timeStamp := time.Now().Local()
	coll := sr.client.Database("book_store_api").Collection("stores")
	filterUpdate := bson.D{{Key: "store_name", Value: storeName}}
	filterFind := bson.D{{Key: "store_name", Value: input.StoreName}}
	data := bson.D{{
		Key: "$set", 
		Value: bson.D{
			{Key: "store_name", Value: input.StoreName},
			{Key: "address", Value: input.Address},
			{Key: "updated_at", Value: timeStamp},
		},
	}}

	_, errUpdate := coll.UpdateOne(ctx, filterUpdate, data)
	if errUpdate != nil {
		return nil, errUpdate
	}

	var result bson.D
	errFind := coll.FindOne(ctx, filterFind).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.Store
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (sr *StoreRepo) DeleteStore(ctx context.Context, storeName string) error {
	coll := sr.client.Database("book_store_api").Collection("stores")
	filter := bson.D{{Key: "store_name", Value: storeName}}

	_, errDelete := coll.DeleteOne(ctx, filter)
	if errDelete != nil {
		return errDelete
	}

	return nil
}

func (sr *StoreRepo) FindStoreById(ctx context.Context, storeId string) (*entity.Store, error) {
	storeObjId, errStoreObjectIDFromHex := primitive.ObjectIDFromHex(storeId)
	if errStoreObjectIDFromHex != nil {
		return nil, errStoreObjectIDFromHex
	}

	coll := sr.client.Database("book_store_api").Collection("stores")
	filter := bson.D{{Key: "_id", Value: storeObjId}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.Store
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (sr *StoreRepo) FindStoreByName(ctx context.Context, storeName string) (*entity.Store, error) {
	coll := sr.client.Database("book_store_api").Collection("stores")
	filter := bson.D{{Key: "store_name", Value: storeName}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.Store
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (sr *StoreRepo) FindAllStore(ctx context.Context) ([]*entity.Store, error) {
	coll := sr.client.Database("book_store_api").Collection("stores")

	cursor, errFind := coll.Find(ctx, bson.D{})
	if errFind != nil {
		return nil, errFind
	}
	defer cursor.Close(ctx)

	var results []bson.D
	errResults := cursor.All(ctx, &results)
	if errResults != nil {
		return nil, errResults
	}

	outputs := make([]*entity.Store, 0)
	for _, result := range results {
		doc, errMarshal := bson.Marshal(result)
		if errMarshal != nil {
			return nil, errMarshal
		}

		var output entity.Store
		errUnmarshal := bson.Unmarshal(doc, &output)
		if errUnmarshal != nil {
			return nil, errUnmarshal
		}

		outputs = append(outputs, &output)
	}

	return outputs, nil
}