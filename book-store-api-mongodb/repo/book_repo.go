package repo

import (
	"book-store-api-mongodb/entity"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type BookRepo struct {
	client *mongo.Client
}

type BookRepoInterface interface {
	CreateBook(ctx context.Context, input *entity.BookInput) (*entity.Book, error)
	UpdateBook(ctx context.Context, input *entity.BookInput, bookTitle string) (*entity.Book, error)
	DeleteBook(ctx context.Context, bookTitle string) error
	FindBookById(ctx context.Context, bookId string) (*entity.Book, error)
	FindBookByTitle(ctx context.Context, bookTitle string) (*entity.Book, error)
	FindAllBook(ctx context.Context) ([]*entity.Book, error)
}

func NewBookRepo(client *mongo.Client) BookRepoInterface {
	return &BookRepo{client: client}
}

func (br *BookRepo) CreateBook(ctx context.Context, input *entity.BookInput) (*entity.Book, error) {
	timeStamp := time.Now().Local()
	coll := br.client.Database("book_store_api").Collection("books")
	data := bson.D{
			{Key: "book_title", Value: input.BookTitle},
			{Key: "book_author", Value: input.BookAuthor},
			{Key: "year_published", Value: input.YearPublished},
			{Key: "price", Value: input.Price},
			{Key: "created_at", Value: timeStamp},
		}

	insert, errInsert := coll.InsertOne(ctx, data)
	if errInsert != nil {
		return nil, errInsert
	}

	output := entity.Book{
		Id: insert.InsertedID.(primitive.ObjectID),
		BookTitle: input.BookTitle,
		BookAuthor: input.BookAuthor,
		YearPublished: input.YearPublished,
		Price: input.Price,
		CreatedAt: &timeStamp,
		UpdatedAt: nil,
	}

	return &output, nil
}

func (br *BookRepo) UpdateBook(ctx context.Context, input *entity.BookInput, bookTitle string) (*entity.Book, error) {
	timeStamp := time.Now().Local()
	coll := br.client.Database("book_store_api").Collection("books")
	filterUpdate := bson.D{{Key: "book_title", Value: bookTitle}}
	filterFind := bson.D{{Key: "book_title", Value: input.BookTitle}}
	data := bson.D{{
		Key: "$set", 
		Value: bson.D{
			{Key: "book_title", Value: input.BookTitle},
			{Key: "book_author", Value: input.BookAuthor},
			{Key: "year_published", Value: input.YearPublished},
			{Key: "price", Value: input.Price},
			{Key: "updated_at", Value: timeStamp},
		},
	}}

	_, errUpdate := coll.UpdateOne(ctx, filterUpdate, data)
	if errUpdate != nil {
		return nil, errUpdate
	}

	var result bson.D
	errFind := coll.FindOne(ctx, filterFind).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.Book
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (br *BookRepo) DeleteBook(ctx context.Context, bookTitle string) error {
	coll := br.client.Database("book_store_api").Collection("books")
	filter := bson.D{{Key: "book_title", Value: bookTitle}}

	_, errDelete := coll.DeleteOne(ctx, filter)
	if errDelete != nil {
		return errDelete
	}

	return nil
}

func (br *BookRepo) FindBookById(ctx context.Context, bookId string) (*entity.Book, error) {
	bookObjId, errBookObjectIDFromHex := primitive.ObjectIDFromHex(bookId)
	if errBookObjectIDFromHex != nil {
		return nil, errBookObjectIDFromHex
	}

	coll := br.client.Database("book_store_api").Collection("books")
	filter := bson.D{{Key: "_id", Value: bookObjId}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.Book
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (br *BookRepo) FindBookByTitle(ctx context.Context, bookTitle string) (*entity.Book, error) {
	coll := br.client.Database("book_store_api").Collection("books")
	filter := bson.D{{Key: "book_title", Value: bookTitle}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.Book
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (br *BookRepo) FindAllBook(ctx context.Context) ([]*entity.Book, error) {
	coll := br.client.Database("book_store_api").Collection("books")

	cursor, errFind := coll.Find(ctx, bson.D{})
	if errFind != nil {
		return nil, errFind
	}
	defer cursor.Close(ctx)

	var results []bson.D
	errResults := cursor.All(ctx, &results)
	if errResults != nil {
		return nil, errResults
	}

	outputs := make([]*entity.Book, 0)
	for _, result := range results {
		doc, errMarshal := bson.Marshal(result)
		if errMarshal != nil {
			return nil, errMarshal
		}

		var output entity.Book
		errUnmarshal := bson.Unmarshal(doc, &output)
		if errUnmarshal != nil {
			return nil, errUnmarshal
		}

		outputs = append(outputs, &output)
	}

	return outputs, nil
}