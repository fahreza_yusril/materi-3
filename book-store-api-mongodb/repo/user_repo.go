package repo

import (
	"book-store-api-mongodb/entity"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserRepo struct {
	client *mongo.Client
}

type UserRepoInterface interface {
	CreateUser(ctx context.Context, input *entity.UserRegisterInput) (*entity.GetUserOutput, error)
	UpdateUser(ctx context.Context, input *entity.UserInput, userId string) (*entity.GetUserOutput, error)
	UpdateUserBalance(ctx context.Context, input *entity.UserTopUpBalanceInput, userId string) (*entity.GetUserOutput, error)
	DeleteUser(ctx context.Context, userId string) error
	FindUserById(ctx context.Context, userId string) (*entity.GetUserOutput, error)
	FindUserByName(ctx context.Context, userName string) (*entity.User, error)
}

func NewUserRepo(client *mongo.Client) UserRepoInterface {
	return &UserRepo{client: client}
}

func (ur *UserRepo) CreateUser(ctx context.Context, input *entity.UserRegisterInput) (*entity.GetUserOutput, error) {
	timeStamp := time.Now().Local()
	coll := ur.client.Database("book_store_api").Collection("users")
	data := bson.D{
			{Key: "user_name", Value: input.UserName},
			{Key: "password", Value: input.Password},
			{Key: "role", Value: input.Role},
			{Key: "balance", Value: 0},
			{Key: "created_at", Value: timeStamp},
		}

	insert, errInsert := coll.InsertOne(ctx, data)
	if errInsert != nil {
		return nil, errInsert
	}

	output := entity.GetUserOutput{
		Id: insert.InsertedID.(primitive.ObjectID),
		UserName: input.UserName,
		Role: input.Role,
		Balance: 0,
		CreatedAt: &timeStamp,
		UpdatedAt: nil,
	}

	return &output, nil
}

func (ur *UserRepo) UpdateUser(ctx context.Context, input *entity.UserInput, userId string) (*entity.GetUserOutput, error) {
	userObjId, errUserObjectIDFromHex := primitive.ObjectIDFromHex(userId)
	if errUserObjectIDFromHex != nil {
		return nil, errUserObjectIDFromHex
	}

	timeStamp := time.Now().Local()
	coll := ur.client.Database("book_store_api").Collection("users")
	filter := bson.D{{Key: "_id", Value: userObjId}}
	data := bson.D{{
		Key: "$set", 
		Value: bson.D{
			{Key: "user_name", Value: input.UserName},
			{Key: "password", Value: input.Password},
			{Key: "updated_at", Value: timeStamp},
		},
	}}

	_, errUpdate := coll.UpdateOne(ctx, filter, data)
	if errUpdate != nil {
		return nil, errUpdate
	}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var decodedResult entity.User
	errUnmarshal := bson.Unmarshal(doc, &decodedResult)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	output := entity.GetUserOutput{
		Id: decodedResult.Id,
		UserName: decodedResult.UserName,
		Role: decodedResult.Role,
		Balance: decodedResult.Balance,
		CreatedAt: decodedResult.CreatedAt,
		UpdatedAt: decodedResult.UpdatedAt,
	}

	return &output, nil
}

func (ur *UserRepo) UpdateUserBalance(ctx context.Context, input *entity.UserTopUpBalanceInput, userId string) (*entity.GetUserOutput, error) {
	userObjId, errUserObjectIDFromHex := primitive.ObjectIDFromHex(userId)
	if errUserObjectIDFromHex != nil {
		return nil, errUserObjectIDFromHex
	}

	timeStamp := time.Now().Local()
	coll := ur.client.Database("book_store_api").Collection("users")
	filter := bson.D{{Key: "_id", Value: userObjId}}
	data := bson.D{{
		Key: "$set", 
		Value: bson.D{
			{Key: "balance", Value: input.Amount},
			{Key: "updated_at", Value: timeStamp},
		},
	}}

	_, errUpdate := coll.UpdateOne(ctx, filter, data)
	if errUpdate != nil {
		return nil, errUpdate
	}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var decodedResult entity.User
	errUnmarshal := bson.Unmarshal(doc, &decodedResult)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	output := entity.GetUserOutput{
		Id: decodedResult.Id,
		UserName: decodedResult.UserName,
		Role: decodedResult.Role,
		Balance: decodedResult.Balance,
		CreatedAt: decodedResult.CreatedAt,
		UpdatedAt: decodedResult.UpdatedAt,
	}

	return &output, nil
}

func (ur *UserRepo) DeleteUser(ctx context.Context, userId string) error {
	userObjId, errUserObjectIDFromHex := primitive.ObjectIDFromHex(userId)
	if errUserObjectIDFromHex != nil {
		return errUserObjectIDFromHex
	}

	coll := ur.client.Database("book_store_api").Collection("users")
	filter := bson.D{{Key: "_id", Value: userObjId}}

	_, errDelete := coll.DeleteOne(ctx, filter)
	if errDelete != nil {
		return errDelete
	}

	return nil
}

func (ur *UserRepo) FindUserById(ctx context.Context, userId string) (*entity.GetUserOutput, error) {
	userObjId, errUserObjectIDFromHex := primitive.ObjectIDFromHex(userId)
	if errUserObjectIDFromHex != nil {
		return nil, errUserObjectIDFromHex
	}

	coll := ur.client.Database("book_store_api").Collection("users")
	filter := bson.D{{Key: "_id", Value: userObjId}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var decodedResult entity.User
	errUnmarshal := bson.Unmarshal(doc, &decodedResult)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	output := entity.GetUserOutput{
		Id: decodedResult.Id,
		UserName: decodedResult.UserName,
		Role: decodedResult.Role,
		Balance: decodedResult.Balance,
		CreatedAt: decodedResult.CreatedAt,
		UpdatedAt: decodedResult.UpdatedAt,
	}

	return &output, nil
}

func (ur *UserRepo) FindUserByName(ctx context.Context, userName string) (*entity.User, error) {
	coll := ur.client.Database("book_store_api").Collection("users")
	filter := bson.D{{Key: "user_name", Value: userName}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.User
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}