package repo

import (
	"book-store-api-mongodb/entity"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type BookBuyerRelationshipRepo struct {
	client *mongo.Client
}

type BookBuyerRelationshipRepoInterface interface {
	CreateBookBuyerRelationship(ctx context.Context, buyerId string, bookId string) (*entity.BookBuyerRelationship, error)
	DeleteBookBuyerRelationship(ctx context.Context, buyerOrBookId string) error
	FindRelationshipByBuyerAndBookId(ctx context.Context, buyerId string, bookId string) (*entity.BookBuyerRelationship, error)
	FindBookListByBuyerId(ctx context.Context, buyerId string) ([]*entity.BookBuyerRelationship, error)
}

func NewBookBuyerRelationshipRepo(client *mongo.Client) BookBuyerRelationshipRepoInterface {
	return &BookBuyerRelationshipRepo{client: client}
}

func (bbrr *BookBuyerRelationshipRepo) CreateBookBuyerRelationship(ctx context.Context, buyerId string, bookId string) (*entity.BookBuyerRelationship, error) {
	buyerObjId, errBuyerObjectIDFromHex := primitive.ObjectIDFromHex(buyerId)
	if errBuyerObjectIDFromHex != nil {
		return nil, errBuyerObjectIDFromHex
	}
	
	bookObjId, errBookObjectIDFromHex := primitive.ObjectIDFromHex(bookId)
	if errBookObjectIDFromHex != nil {
		return nil, errBookObjectIDFromHex
	}
	
	timeStamp := time.Now().Local()
	coll := bbrr.client.Database("book_store_api").Collection("book_buyer_relationships")
	data := bson.D{
			{Key: "buyer_id", Value: buyerObjId},
			{Key: "book_id", Value: bookObjId},
			{Key: "created_at", Value: timeStamp},
		}

	insert, errInsert := coll.InsertOne(ctx, data)
	if errInsert != nil {
		return nil, errInsert
	}

	output := entity.BookBuyerRelationship{
		Id: insert.InsertedID.(primitive.ObjectID),
		BuyerId: buyerObjId,
		BookId: bookObjId,
		CreatedAt: &timeStamp,
	}

	return &output, nil
}

func (bbrr *BookBuyerRelationshipRepo) DeleteBookBuyerRelationship(ctx context.Context, buyerOrBookId string) error {
	buyerOrBookObjId, errBuyerOrBookObjectIDFromHex := primitive.ObjectIDFromHex(buyerOrBookId)
	if errBuyerOrBookObjectIDFromHex != nil {
		return errBuyerOrBookObjectIDFromHex
	}
	
	coll := bbrr.client.Database("book_store_api").Collection("book_buyer_relationships")
	filter := bson.D{{Key: "$or", Value: bson.A{bson.D{{Key: "buyer_id", Value: buyerOrBookObjId}}, bson.D{{Key: "book_id", Value: buyerOrBookObjId}}}}}

	_, errDelete := coll.DeleteOne(ctx, filter)
	if errDelete != nil {
		return errDelete
	}

	return nil
}

func (bbrr *BookBuyerRelationshipRepo) FindRelationshipByBuyerAndBookId(ctx context.Context, buyerId string, bookId string) (*entity.BookBuyerRelationship, error) {
	buyerObjId, errBuyerObjectIDFromHex := primitive.ObjectIDFromHex(buyerId)
	if errBuyerObjectIDFromHex != nil {
		return nil, errBuyerObjectIDFromHex
	}
	
	bookObjId, errBookObjectIDFromHex := primitive.ObjectIDFromHex(bookId)
	if errBookObjectIDFromHex != nil {
		return nil, errBookObjectIDFromHex
	}
	
	coll := bbrr.client.Database("book_store_api").Collection("book_buyer_relationships")
	filter := bson.D{{Key: "$and", Value: bson.A{bson.D{{Key: "buyer_id", Value: buyerObjId}}, bson.D{{Key: "book_id", Value: bookObjId}}}}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.BookBuyerRelationship
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (bbrr *BookBuyerRelationshipRepo) FindBookListByBuyerId(ctx context.Context, buyerId string) ([]*entity.BookBuyerRelationship, error) {
	buyerObjId, errBuyerObjectIDFromHex := primitive.ObjectIDFromHex(buyerId)
	if errBuyerObjectIDFromHex != nil {
		return nil, errBuyerObjectIDFromHex
	}
	
	coll := bbrr.client.Database("book_store_api").Collection("book_buyer_relationships")
	filter := bson.D{{Key: "buyer_id", Value: buyerObjId}}

	cursor, errFind := coll.Find(ctx, filter)
	if errFind != nil {
		return nil, errFind
	}
	defer cursor.Close(ctx)

	var results []bson.D
	errResults := cursor.All(ctx, &results)
	if errResults != nil {
		return nil, errResults
	}

	outputs := make([]*entity.BookBuyerRelationship, 0)
	for _, result := range results {
		doc, errMarshal := bson.Marshal(result)
		if errMarshal != nil {
			return nil, errMarshal
		}

		var output entity.BookBuyerRelationship
		errUnmarshal := bson.Unmarshal(doc, &output)
		if errUnmarshal != nil {
			return nil, errUnmarshal
		}

		outputs = append(outputs, &output)
	}

	return outputs, nil
}