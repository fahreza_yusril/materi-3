package repo

import (
	"book-store-api-mongodb/entity"
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type MerchantStoreRelationshipRepo struct {
	client *mongo.Client
}

type MerchantStoreRelationshipRepoInterface interface {
	CreateMerchantStoreRelationship(ctx context.Context, merchantId string, storeId string) (*entity.MerchantStoreRelationship, error)
	DeleteMerchantStoreRelationship(ctx context.Context, merchantOrStoreId string) error
	FindMerchantByStoreId(ctx context.Context, storeId string) (*entity.MerchantStoreRelationship, error)
	FindStoreByMerchantId(ctx context.Context, merchantId string) (*entity.MerchantStoreRelationship, error)
}

func NewMerchantStoreRelationshipRepo(client *mongo.Client) MerchantStoreRelationshipRepoInterface {
	return &MerchantStoreRelationshipRepo{client: client}
}

func (msrr *MerchantStoreRelationshipRepo) CreateMerchantStoreRelationship(ctx context.Context, merchantId string, storeId string) (*entity.MerchantStoreRelationship, error) {
	merchantObjId, errMerchantObjectIDFromHex := primitive.ObjectIDFromHex(merchantId)
	if errMerchantObjectIDFromHex != nil {
		return nil, errMerchantObjectIDFromHex
	}
	
	storeObjId, errStoreObjectIDFromHex := primitive.ObjectIDFromHex(storeId)
	if errStoreObjectIDFromHex != nil {
		return nil, errStoreObjectIDFromHex
	}
	
	timeStamp := time.Now().Local()
	coll := msrr.client.Database("book_store_api").Collection("merchant_store_relationships")
	data := bson.D{
			{Key: "merchant_id", Value: merchantObjId},
			{Key: "store_id", Value: storeObjId},
			{Key: "created_at", Value: timeStamp},
		}

	insert, errInsert := coll.InsertOne(ctx, data)
	if errInsert != nil {
		return nil, errInsert
	}

	output := entity.MerchantStoreRelationship{
		Id: insert.InsertedID.(primitive.ObjectID),
		MerchantId: merchantObjId,
		StoreId: storeObjId,
		CreatedAt: &timeStamp,
	}

	return &output, nil
}

func (msrr *MerchantStoreRelationshipRepo) DeleteMerchantStoreRelationship(ctx context.Context, merchantOrStoreId string) error {
	merchantOrStoreObjId, errMerchantOrStoreObjectIDFromHex := primitive.ObjectIDFromHex(merchantOrStoreId)
	if errMerchantOrStoreObjectIDFromHex != nil {
		return errMerchantOrStoreObjectIDFromHex
	}
	
	coll := msrr.client.Database("book_store_api").Collection("merchant_store_relationships")
	filter := bson.D{{Key: "$or", Value: bson.A{bson.D{{Key: "merchant_id", Value: merchantOrStoreObjId}}, bson.D{{Key: "store_id", Value: merchantOrStoreObjId}}}}}

	_, errDelete := coll.DeleteOne(ctx, filter)
	if errDelete != nil {
		return errDelete
	}

	return nil
}

func (msrr *MerchantStoreRelationshipRepo) FindMerchantByStoreId(ctx context.Context, storeId string) (*entity.MerchantStoreRelationship, error) {
	storeObjId, errStoreObjectIDFromHex := primitive.ObjectIDFromHex(storeId)
	if errStoreObjectIDFromHex != nil {
		return nil, errStoreObjectIDFromHex
	}
	
	coll := msrr.client.Database("book_store_api").Collection("merchant_store_relationships")
	filter := bson.D{{Key: "store_id", Value: storeObjId}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.MerchantStoreRelationship
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}

func (msrr *MerchantStoreRelationshipRepo) FindStoreByMerchantId(ctx context.Context, merchantId string) (*entity.MerchantStoreRelationship, error) {
	merchantObjId, errMerchantObjectIDFromHex := primitive.ObjectIDFromHex(merchantId)
	if errMerchantObjectIDFromHex != nil {
		return nil, errMerchantObjectIDFromHex
	}
	
	coll := msrr.client.Database("book_store_api").Collection("merchant_store_relationships")
	filter := bson.D{{Key: "merchant_id", Value: merchantObjId}}

	var result bson.D
	errFind := coll.FindOne(ctx, filter).Decode(&result)
	if errFind != nil {
		return nil, errFind
	}

	doc, errMarshal := bson.Marshal(result)
	if errMarshal != nil {
		return nil, errMarshal
	}

	var output entity.MerchantStoreRelationship
	errUnmarshal := bson.Unmarshal(doc, &output)
	if errUnmarshal != nil {
		return nil, errUnmarshal
	}

	return &output, nil
}