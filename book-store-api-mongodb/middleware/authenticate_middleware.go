package middleware

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go/v4"
)

func Authenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		secret := []byte("TokenSecret")	
		authStr := strings.TrimPrefix(r.Header.Get("Authorization"), "Bearer ")
		if len(authStr) == 0 {
			http.Error(w, "Unauthorized", http.StatusForbidden)
		}

		token, err := jwt.Parse(authStr, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Parsing error!")
			}
			return secret, nil
		})
		if err != nil {
			http.Error(w, "Unauthorized", http.StatusForbidden)
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			r.Header.Set("Id", claims["_id"].(string))
			next.ServeHTTP(w, r)
		}
	}) 
}