package service

import (
	"book-store-api-mongodb/entity"
	"book-store-api-mongodb/repo"
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
)

type StoreService struct {
	bookRepo 						*repo.BookRepo
	storeRepo						*repo.StoreRepo
	userRepo 						*repo.UserRepo
	bookStoreRelationshipRepo 		*repo.BookStoreRelationshipRepo
	merchantStoreRelationshipRepo	*repo.MerchantStoreRelationshipRepo
	client							*mongo.Client
}

type StoreServiceInterface interface {
	CreateStore(ctx context.Context, input *entity.StoreInput, merchantId string) (*entity.Store, error)
	UpdateStore(ctx context.Context, input *entity.UpdateStoreInput, merchantId string) (*entity.Store, error)
	UpdateBookStock(ctx context.Context, input *entity.BookListInput, merchantId string) (*entity.UpdateBookStock, error)
	DeleteStore(ctx context.Context, merchantId string) error
	FindStoreByName(ctx context.Context, storeName string) (*entity.Store, error)
	FindAllStore(ctx context.Context) ([]*entity.Store, error)
	FindStoreListByBook(ctx context.Context, bookTitle string) (*entity.StoreListByBook, error)
	FindStoreByMerchant(ctx context.Context, merchantId string) (*entity.StoreByMerchant, error)
}

func NewStoreService(bookRepo *repo.BookRepo, storeRepo *repo.StoreRepo, userRepo *repo.UserRepo, bookStoreRelationshipRepo *repo.BookStoreRelationshipRepo, merchantStoreRelationshipRepo *repo.MerchantStoreRelationshipRepo, client *mongo.Client) StoreServiceInterface {
	return &StoreService{
		bookRepo: bookRepo,
		storeRepo: storeRepo,
		userRepo: userRepo,
		bookStoreRelationshipRepo: bookStoreRelationshipRepo,
		merchantStoreRelationshipRepo: merchantStoreRelationshipRepo,
		client: client,
	}
}

func (ss *StoreService) CreateStore(ctx context.Context, input *entity.StoreInput, merchantId string) (*entity.Store, error) {
	resultFindUserById, errFindUserById := ss.userRepo.FindUserById(ctx, merchantId)
	if errFindUserById != nil {
		return nil, errFindUserById
	}

	if resultFindUserById.Role != "Merchant" {
		return nil, errors.New("User tidak diizinkan!")
	}
	
	_, errFindStoreByName := ss.storeRepo.FindStoreByName(ctx, input.StoreName)
	if errFindStoreByName == nil {
		return nil, errors.New("Nama toko sudah ada!")
	}

	opts := options.Session().SetDefaultReadConcern(readconcern.Majority())
	sess, errStartSess := ss.client.StartSession(opts)
	if errStartSess != nil {
		return nil, errStartSess
	}
	defer sess.EndSession(ctx)

	errWithSess := mongo.WithSession(ctx, sess, func(sessCtx mongo.SessionContext) error {
		if errStartTransaction := sess.StartTransaction(); errStartTransaction != nil {
			return errStartTransaction
		}

		resultCreateStore, errCreateStore := ss.storeRepo.CreateStore(sessCtx, input)
		if errCreateStore != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errCreateStore
		}

		_, errCreateMerchantStoreRelationship := ss.merchantStoreRelationshipRepo.CreateMerchantStoreRelationship(sessCtx, merchantId, resultCreateStore.Id.Hex())
		if errCreateMerchantStoreRelationship != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errCreateMerchantStoreRelationship
		}

		for _, book := range input.BookList {
			resultFindBookByTitle, errFindBookByTitle := ss.bookRepo.FindBookByTitle(sessCtx, book.BookTitle)
			if errFindBookByTitle != nil {
				errRb := sess.AbortTransaction(ctx)
				if errRb != nil {
					return errRb
				}
				return errFindBookByTitle
			}

			_, errCreateBookStoreRelationship := ss.bookStoreRelationshipRepo.CreateBookStoreRelationship(sessCtx, resultCreateStore.Id.Hex(), resultFindBookByTitle.Id.Hex(), book.Stock)
			if errCreateBookStoreRelationship != nil {
				errRb := sess.AbortTransaction(ctx)
				if errRb != nil {
					return errRb
				}
				return errCreateBookStoreRelationship
			}
		}

		return sess.CommitTransaction(ctx)
	})
	if errWithSess != nil {
		return nil, errWithSess
	}

	output, errFindStoreByName := ss.storeRepo.FindStoreByName(ctx, input.StoreName)
	if errFindStoreByName != nil {
		return nil, errFindStoreByName
	}

	return output, nil
}

func (ss *StoreService) UpdateStore(ctx context.Context, input *entity.UpdateStoreInput, merchantId string) (*entity.Store, error) {
	resultFindUserById, errFindUserById := ss.userRepo.FindUserById(ctx, merchantId)
	if errFindUserById != nil {
		return nil, errFindUserById
	}

	if resultFindUserById.Role != "Merchant" {
		return nil, errors.New("User tidak diizinkan!")
	}

	resultFindStoreByMerchantId, errFindStoreByMerchantId := ss.merchantStoreRelationshipRepo.FindStoreByMerchantId(ctx, merchantId)
	if errFindStoreByMerchantId != nil {
		return nil, errFindStoreByMerchantId
	}

	resultFindStoreById, errFindStoreById := ss.storeRepo.FindStoreById(ctx, resultFindStoreByMerchantId.StoreId.Hex())
	if errFindStoreById != nil {
		return nil, errFindStoreById
	}

	output, errUpdateStore := ss.storeRepo.UpdateStore(ctx, input, resultFindStoreById.StoreName)
	if errUpdateStore != nil {
		return nil, errUpdateStore
	}

	return output, nil
}

func (ss *StoreService) UpdateBookStock(ctx context.Context, input *entity.BookListInput, merchantId string) (*entity.UpdateBookStock, error) {
	resultFindUserById, errFindUserById := ss.userRepo.FindUserById(ctx, merchantId)
	if errFindUserById != nil {
		return nil, errFindUserById
	}

	if resultFindUserById.Role != "Merchant" {
		return nil, errors.New("User tidak diizinkan!")
	}

	resultFindStoreByMerchantId, errFindStoreByMerchantId := ss.merchantStoreRelationshipRepo.FindStoreByMerchantId(ctx, merchantId)
	if errFindStoreByMerchantId != nil {
		return nil, errFindStoreByMerchantId
	}

	resultFindStoreById, errFindStoreById := ss.storeRepo.FindStoreById(ctx, resultFindStoreByMerchantId.StoreId.Hex())
	if errFindStoreById != nil {
		return nil, errFindStoreById
	}

	resultFindBookByTitle, errFindBookByTitle := ss.bookRepo.FindBookByTitle(ctx, input.BookTitle)
	if errFindBookByTitle != nil {
		return nil, errFindBookByTitle
	}

	_, errFindRelationshipByStoreAndBookId := ss.bookStoreRelationshipRepo.FindRelationshipByStoreAndBookId(ctx, resultFindStoreById.Id.Hex(), resultFindBookByTitle.Id.Hex())
	if errFindRelationshipByStoreAndBookId != nil {
		return nil, errFindRelationshipByStoreAndBookId
	}

	_, errUpdateBookStock := ss.bookStoreRelationshipRepo.UpdateBookStock(ctx, resultFindStoreById.Id.Hex(), resultFindBookByTitle.Id.Hex(), input.Stock)
	if errUpdateBookStock != nil {
		return nil, errUpdateBookStock
	}

	output := entity.UpdateBookStock {
		StoreName: resultFindStoreById.StoreName,
		BookTitle: input.BookTitle,
		Stock: input.Stock,
	}

	return &output, nil
}

func (ss *StoreService) DeleteStore(ctx context.Context, merchantId string) error {
	resultFindUserById, errFindUserById := ss.userRepo.FindUserById(ctx, merchantId)
	if errFindUserById != nil {
		return errFindUserById
	}

	if resultFindUserById.Role != "Merchant" {
		return errors.New("User tidak diizinkan!")
	}

	resultFindStoreByMerchantId, errFindStoreByMerchantId := ss.merchantStoreRelationshipRepo.FindStoreByMerchantId(ctx, merchantId)
	if errFindStoreByMerchantId != nil {
		return errFindStoreByMerchantId
	}

	resultFindStoreById, errFindStoreById := ss.storeRepo.FindStoreById(ctx, resultFindStoreByMerchantId.StoreId.Hex())
	if errFindStoreById != nil {
		return errFindStoreById
	}

	opts := options.Session().SetDefaultReadConcern(readconcern.Majority())
	sess, errStartSess := ss.client.StartSession(opts)
	if errStartSess != nil {
		return errStartSess
	}
	defer sess.EndSession(ctx)

	errWithSess := mongo.WithSession(ctx, sess, func(sessCtx mongo.SessionContext) error {
		if errStartTransaction := sess.StartTransaction(); errStartTransaction != nil {
			return errStartTransaction
		}

		errDeleteStore := ss.storeRepo.DeleteStore(sessCtx, resultFindStoreById.StoreName)
		if errDeleteStore != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errDeleteStore
		}

		errBookStoreDeleteRelationship := ss.bookStoreRelationshipRepo.DeleteBookStoreRelationship(sessCtx, resultFindStoreById.Id.Hex())
		if errBookStoreDeleteRelationship != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errBookStoreDeleteRelationship
		}

		return sess.CommitTransaction(ctx)
	})
	if errWithSess != nil {
		return errWithSess
	}
	
	return nil
}

func (ss *StoreService) FindStoreByName(ctx context.Context, storeName string) (*entity.Store, error) {
	output, errFindStoreByName := ss.storeRepo.FindStoreByName(ctx, storeName)
	if errFindStoreByName != nil {
		return nil, errFindStoreByName
	}
	return output, nil
}

func (ss *StoreService) FindAllStore(ctx context.Context) ([]*entity.Store, error) {
	outputs, errFindAllStore := ss.storeRepo.FindAllStore(ctx)
	if errFindAllStore != nil {
		return nil, errFindAllStore
	}
	return outputs, nil
}

func (ss *StoreService) FindStoreListByBook(ctx context.Context, bookTitle string) (*entity.StoreListByBook, error) {
	resultFindBookByTitle, errFindBookByTitle := ss.bookRepo.FindBookByTitle(ctx, bookTitle)
	if errFindBookByTitle != nil {
		return nil, errFindBookByTitle
	}

	resultsFindStoreListByBookId, errFindStoreListByBookId := ss.bookStoreRelationshipRepo.FindStoreListByBookId(ctx, resultFindBookByTitle.Id.Hex())
	if errFindStoreListByBookId != nil {
		return nil, errFindStoreListByBookId
	}

	storeList := make([]entity.StoreData, 0)
	for _, store := range resultsFindStoreListByBookId {
		resultFindStoreById, errFindStoreById := ss.storeRepo.FindStoreById(ctx, store.StoreId.Hex())
		if errFindStoreById != nil {
			return nil, errFindStoreById
		}

		storeData := entity.StoreData{
			StoreName: resultFindStoreById.StoreName,
			Address: resultFindStoreById.Address,
			Stock: store.Stock,
		}

		storeList = append(storeList, storeData)
	}

	output := entity.StoreListByBook{
		BookTitle: resultFindBookByTitle.BookTitle,
		BookAuthor: resultFindBookByTitle.BookAuthor,
		YearPublished: resultFindBookByTitle.YearPublished,
		Price: resultFindBookByTitle.Price,
		StoreList: storeList,
	}

	return &output, nil
}

func (ss *StoreService) FindStoreByMerchant(ctx context.Context, merchantId string) (*entity.StoreByMerchant, error) {
	resultFindUserById, errFindUserById := ss.userRepo.FindUserById(ctx, merchantId)
	if errFindUserById != nil {
		return nil, errFindUserById
	}

	if resultFindUserById.Role != "Merchant" {
		return nil, errors.New("User tidak diizinkan!")
	}

	resultFindStoreByMerchantId, errFindStoreByMerchantId := ss.merchantStoreRelationshipRepo.FindStoreByMerchantId(ctx, merchantId)
	if errFindStoreByMerchantId != nil {
		return nil, errFindStoreByMerchantId
	}

	resultFindStoreById, errFindStoreById := ss.storeRepo.FindStoreById(ctx, resultFindStoreByMerchantId.StoreId.Hex())
	if errFindStoreById != nil {
		return nil, errFindStoreById
	}

	merchantStoreData := entity.MerchantStoreData{
		StoreName: resultFindStoreById.StoreName,
		Address: resultFindStoreById.Address,
	}

	output := entity.StoreByMerchant{
		UserName: resultFindUserById.UserName,
		Role: resultFindUserById.Role,
		Store: merchantStoreData,
	}

	return &output, nil
}