package service

import (
	"book-store-api-mongodb/entity"
	"book-store-api-mongodb/repo"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"time"

	jwt "github.com/dgrijalva/jwt-go/v4"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
)

type UserService struct {
	userRepo 						*repo.UserRepo
	bookBuyerRelationshipRepo 		*repo.BookBuyerRelationshipRepo
	merchantStoreRelationshipRepo	*repo.MerchantStoreRelationshipRepo
	client							*mongo.Client
}

type UserServiceInterface interface {
	RegisterUser(ctx context.Context, input *entity.UserRegisterInput) (*entity.TokenOutput, error)
	LoginUser(ctx context.Context, input *entity.UserInput) (*entity.TokenOutput, error)
	UpdateUser(ctx context.Context, input *entity.UserInput, userId string) (*entity.GetUserOutput, error)
	TopUpBalance(ctx context.Context, input *entity.UserTopUpBalanceInput, userId string) (*entity.GetUserOutput, error)
	DeleteUser(ctx context.Context, userId string) error
	FindUserById(ctx context.Context, userId string) (*entity.GetUserOutput, error)
	RefreshToken(ctx context.Context, userId string) (*entity.TokenOutput, error)
}

func NewUserService(userRepo *repo.UserRepo, bookBuyerRelationshipRepo *repo.BookBuyerRelationshipRepo, merchantStoreRelationshipRepo *repo.MerchantStoreRelationshipRepo, client *mongo.Client) UserServiceInterface {
	return &UserService{
		userRepo: userRepo,
		bookBuyerRelationshipRepo: bookBuyerRelationshipRepo,
		merchantStoreRelationshipRepo: merchantStoreRelationshipRepo,
		client: client,
	}
}

func (us *UserService) RegisterUser(ctx context.Context, input *entity.UserRegisterInput) (*entity.TokenOutput, error) {
	_, errFindUserByName := us.userRepo.FindUserByName(ctx, input.UserName)
	if errFindUserByName == nil {
		return nil, errors.New("Username sudah ada!")
	}

	hashedPassword := sha256.Sum256([]byte(input.Password))

	userInput := entity.UserRegisterInput{
		UserName	: input.UserName,
		Password	: hex.EncodeToString(hashedPassword[:]),
		Role		: input.Role,	
	}

	resultCreateUser, errCreateUser := us.userRepo.CreateUser(ctx, &userInput)
	if errCreateUser != nil {
		return nil, errCreateUser
	}

	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, entity.TokenClaims{
		Id				: resultCreateUser.Id.Hex(),
		StandardClaims	: jwt.StandardClaims{
							ExpiresAt : &jwt.Time{
								Time : time.Now().Add(time.Hour * 6).Local(),
							},
		},
	})
	signedAccessToken, errAccessToken := accessToken.SignedString([]byte("TokenSecret"))
	if errAccessToken != nil {
		return nil, errAccessToken
	}

	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, entity.TokenClaims{
		Id				: resultCreateUser.Id.Hex(),
		StandardClaims	: jwt.StandardClaims{
							ExpiresAt : &jwt.Time{
								Time : time.Now().Add(time.Hour * 168).Local(),
							},
		},
	})
	signedRefreshToken, errRefreshToken := refreshToken.SignedString([]byte("TokenSecret"))
	if errRefreshToken != nil {
		return nil, errRefreshToken
	}

	tokenOutput := entity.TokenOutput{
		AccessToken		: signedAccessToken,
		RefreshToken	: signedRefreshToken,
	}

	return &tokenOutput, nil
}

func (us *UserService) LoginUser(ctx context.Context, input *entity.UserInput) (*entity.TokenOutput, error) {
	resultFindUserByName, errFindUserByName := us.userRepo.FindUserByName(ctx, input.UserName)
	if errFindUserByName != nil {
		return nil, errFindUserByName
	}

	hashedPassword := sha256.Sum256([]byte(input.Password))

	if resultFindUserByName.Password != hex.EncodeToString(hashedPassword[:]) {
		return nil, errors.New("Password tidak sesuai!")
	}

	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, entity.TokenClaims{
		Id				: resultFindUserByName.Id.Hex(),
		StandardClaims	: jwt.StandardClaims{
							ExpiresAt : &jwt.Time{
								Time : time.Now().Add(time.Hour * 6).Local(),
							},
		},
	})
	signedAccessToken, errAccessToken := accessToken.SignedString([]byte("TokenSecret"))
	if errAccessToken != nil {
		return nil, errAccessToken
	}

	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, entity.TokenClaims{
		Id				: resultFindUserByName.Id.Hex(),
		StandardClaims	: jwt.StandardClaims{
							ExpiresAt : &jwt.Time{
								Time : time.Now().Add(time.Hour * 168).Local(),
							},
		},
	})
	signedRefreshToken, errRefreshToken := refreshToken.SignedString([]byte("TokenSecret"))
	if errRefreshToken != nil {
		return nil, errRefreshToken
	}

	tokenOutput := entity.TokenOutput{
		AccessToken		: signedAccessToken,
		RefreshToken	: signedRefreshToken,
	}

	return &tokenOutput, nil
}

func (us *UserService) UpdateUser(ctx context.Context, input *entity.UserInput, userId string) (*entity.GetUserOutput, error) {
	_, errFindUserById := us.userRepo.FindUserById(ctx, userId)
	if errFindUserById != nil {
		return nil, errFindUserById
	}

	hashedPassword := sha256.Sum256([]byte(input.Password))

	userInput := entity.UserInput{
		UserName	: input.UserName,
		Password	: hex.EncodeToString(hashedPassword[:]),	
	}

	resultUpdateUser, errUpdateUser := us.userRepo.UpdateUser(ctx, &userInput, userId)
	if errUpdateUser != nil {
		return nil, errUpdateUser
	}

	return resultUpdateUser, nil
}

func (us *UserService) TopUpBalance(ctx context.Context, input *entity.UserTopUpBalanceInput, userId string) (*entity.GetUserOutput, error) {
	_, errFindUserById := us.userRepo.FindUserById(ctx, userId)
	if errFindUserById != nil {
		return nil, errFindUserById
	}

	resultUpdateUserBalance, errUpdateUserBalance := us.userRepo.UpdateUserBalance(ctx, input, userId)
	if errUpdateUserBalance != nil {
		return nil, errUpdateUserBalance
	}

	return resultUpdateUserBalance, nil
}

func (us *UserService) DeleteUser(ctx context.Context, userId string) error {
	_, errFindUserById := us.userRepo.FindUserById(ctx, userId)
	if errFindUserById != nil {
		return errFindUserById
	}

	opts := options.Session().SetDefaultReadConcern(readconcern.Majority())
	sess, errStartSess := us.client.StartSession(opts)
	if errStartSess != nil {
		return errStartSess
	}
	defer sess.EndSession(ctx)

	errWithSess := mongo.WithSession(ctx, sess, func(sessCtx mongo.SessionContext) error {
		if errStartTransaction := sess.StartTransaction(); errStartTransaction != nil {
			return errStartTransaction
		}

		errDeleteUser := us.userRepo.DeleteUser(ctx, userId)
		if errDeleteUser != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errDeleteUser
		}

		errBookStoreDeleteRelationship := us.bookBuyerRelationshipRepo.DeleteBookBuyerRelationship(sessCtx, userId)
		if errBookStoreDeleteRelationship != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errBookStoreDeleteRelationship
		}

		errBookBuyerDeleteRelationship := us.merchantStoreRelationshipRepo.DeleteMerchantStoreRelationship(sessCtx, userId)
		if errBookBuyerDeleteRelationship != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errBookBuyerDeleteRelationship
		}

		return sess.CommitTransaction(ctx)
	})
	if errWithSess != nil {
		return errWithSess
	}
	
	return nil
}

func (us *UserService) FindUserById(ctx context.Context, userId string) (*entity.GetUserOutput, error) {
	resultFindUserById, errFindUserById := us.userRepo.FindUserById(ctx, userId)
	if errFindUserById != nil {
		return nil, errFindUserById
	}
	return resultFindUserById, nil
}

func (us *UserService) RefreshToken(ctx context.Context, userId string) (*entity.TokenOutput, error) {
	newAccessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, entity.TokenClaims{
		Id				: userId,
		StandardClaims	: jwt.StandardClaims{
							ExpiresAt : &jwt.Time{
								Time : time.Now().Add(time.Hour * 6).Local(),
							},
		},
	})
	newSignedAccessToken, errAccessToken := newAccessToken.SignedString([]byte("TokenSecret"))
	if errAccessToken != nil {
		return nil, errAccessToken
	}

	newRefreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, entity.TokenClaims{
		Id				: userId,
		StandardClaims	: jwt.StandardClaims{
							ExpiresAt : &jwt.Time{
								Time : time.Now().Add(time.Hour * 168).Local(),
							},
		},
	})
	newSignedRefreshToken, errRefreshToken := newRefreshToken.SignedString([]byte("TokenSecret"))
	if errRefreshToken != nil {
		return nil, errRefreshToken
	}

	newTokenOutput := entity.TokenOutput{
		AccessToken		: newSignedAccessToken,
		RefreshToken	: newSignedRefreshToken,
	}

	return &newTokenOutput, nil
}