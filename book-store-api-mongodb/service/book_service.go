package service

import (
	"book-store-api-mongodb/entity"
	"book-store-api-mongodb/repo"
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
)

type BookService struct {
	bookRepo 						*repo.BookRepo
	storeRepo						*repo.StoreRepo
	userRepo 						*repo.UserRepo
	bookStoreRelationshipRepo 		*repo.BookStoreRelationshipRepo
	bookBuyerRelationshipRepo 		*repo.BookBuyerRelationshipRepo
	merchantStoreRelationshipRepo	*repo.MerchantStoreRelationshipRepo
	client							*mongo.Client
}

type BookServiceInterface interface {
	CreateBook(ctx context.Context, input *entity.BookInput) (*entity.Book, error)
	UpdateBook(ctx context.Context, input *entity.BookInput, bookTitle string) (*entity.Book, error)
	DeleteBook(ctx context.Context, bookTitle string) error
	FindBookByTitle(ctx context.Context, bookTitle string) (*entity.Book, error)
	FindAllBook(ctx context.Context) ([]*entity.Book, error)
	FindBookListByStore(ctx context.Context, storeName string) (*entity.BookListByStore, error)
	OrderBook(ctx context.Context, input *entity.OrderBookInput, buyerId string) (*entity.BookListByBuyer, error)
	FindBookListByBuyer(ctx context.Context, buyerId string) (*entity.BookListByBuyer, error)
}

func NewBookService(bookRepo *repo.BookRepo, storeRepo *repo.StoreRepo, userRepo *repo.UserRepo, bookStoreRelationshipRepo *repo.BookStoreRelationshipRepo, bookBuyerRelationshipRepo *repo.BookBuyerRelationshipRepo, merchantStoreRelationshipRepo *repo.MerchantStoreRelationshipRepo, client *mongo.Client) BookServiceInterface {
	return &BookService{
		bookRepo: bookRepo,
		storeRepo: storeRepo,
		userRepo: userRepo,
		bookStoreRelationshipRepo: bookStoreRelationshipRepo,
		bookBuyerRelationshipRepo: bookBuyerRelationshipRepo,
		merchantStoreRelationshipRepo: merchantStoreRelationshipRepo,
		client: client,
	}
}

func (bs *BookService) CreateBook(ctx context.Context, input *entity.BookInput) (*entity.Book, error) {
	_, errFindBookByTitle := bs.bookRepo.FindBookByTitle(ctx, input.BookTitle)
	if errFindBookByTitle == nil {
		return nil, errors.New("Buku sudah ada!")
	}

	output, errCreateBook := bs.bookRepo.CreateBook(ctx, input)
	if errCreateBook != nil {
		return nil, errCreateBook
	}

	return output, nil
}

func (bs *BookService) UpdateBook(ctx context.Context, input *entity.BookInput, bookTitle string) (*entity.Book, error) {
	_, errFindBookByTitle := bs.bookRepo.FindBookByTitle(ctx, bookTitle)
	if errFindBookByTitle != nil {
		return nil, errFindBookByTitle
	}

	output, errUpdateBook := bs.bookRepo.UpdateBook(ctx, input, bookTitle)
	if errUpdateBook != nil {
		return nil, errUpdateBook
	}

	return output, nil
}

func (bs *BookService) DeleteBook(ctx context.Context, bookTitle string) error {
	resultFindBookByTitle, errFindBookByTitle := bs.bookRepo.FindBookByTitle(ctx, bookTitle)
	if errFindBookByTitle != nil {
		return errFindBookByTitle
	}

	opts := options.Session().SetDefaultReadConcern(readconcern.Majority())
	sess, errStartSess := bs.client.StartSession(opts)
	if errStartSess != nil {
		return errStartSess
	}
	defer sess.EndSession(ctx)

	errWithSess := mongo.WithSession(ctx, sess, func(sessCtx mongo.SessionContext) error {
		if errStartTransaction := sess.StartTransaction(); errStartTransaction != nil {
			return errStartTransaction
		}

		errDeleteBook := bs.bookRepo.DeleteBook(sessCtx, bookTitle)
		if errDeleteBook != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errDeleteBook
		}

		errBookStoreDeleteRelationship := bs.bookStoreRelationshipRepo.DeleteBookStoreRelationship(sessCtx, resultFindBookByTitle.Id.Hex())
		if errBookStoreDeleteRelationship != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errBookStoreDeleteRelationship
		}

		errBookBuyerDeleteRelationship := bs.bookBuyerRelationshipRepo.DeleteBookBuyerRelationship(sessCtx, resultFindBookByTitle.Id.Hex())
		if errBookBuyerDeleteRelationship != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errBookBuyerDeleteRelationship
		}

		return sess.CommitTransaction(ctx)
	})
	if errWithSess != nil {
		return errWithSess
	}
	
	return nil
}

func (bs *BookService) FindBookByTitle(ctx context.Context, bookTitle string) (*entity.Book, error) {
	output, errFindBookByTitle := bs.bookRepo.FindBookByTitle(ctx, bookTitle)
	if errFindBookByTitle != nil {
		return nil, errFindBookByTitle
	}
	return output, nil
}

func (bs *BookService) FindAllBook(ctx context.Context) ([]*entity.Book, error) {
	outputs, errFindAllBook := bs.bookRepo.FindAllBook(ctx)
	if errFindAllBook != nil {
		return nil, errFindAllBook
	}
	return outputs, nil
}

func (bs *BookService) FindBookListByStore(ctx context.Context, storeName string) (*entity.BookListByStore, error) {
	resultFindStoreByName, errFindStoreByName := bs.storeRepo.FindStoreByName(ctx, storeName)
	if errFindStoreByName != nil {
		return nil, errFindStoreByName
	}

	resultsFindBookListByStoreId, errFindBookListByStoreId := bs.bookStoreRelationshipRepo.FindBookListByStoreId(ctx, resultFindStoreByName.Id.Hex())
	if errFindBookListByStoreId != nil {
		return nil, errFindBookListByStoreId
	}

	bookList := make([]entity.BookData, 0)
	for _, book := range resultsFindBookListByStoreId {
		resultFindBookById, errFindBookById := bs.bookRepo.FindBookById(ctx, book.BookId.Hex())
		if errFindBookById != nil {
			return nil, errFindBookById
		}

		bookData := entity.BookData{
			BookTitle: resultFindBookById.BookTitle,
			BookAuthor: resultFindBookById.BookAuthor,
			YearPublished: resultFindBookById.YearPublished,
			Price: resultFindBookById.Price,
			Stock: book.Stock,
		}

		bookList = append(bookList, bookData)
	}

	output := entity.BookListByStore{
		StoreName: resultFindStoreByName.StoreName,
		Address: resultFindStoreByName.Address,
		BookList: bookList,
	}

	return &output, nil
}

func (bs *BookService) OrderBook(ctx context.Context, input *entity.OrderBookInput, buyerId string) (*entity.BookListByBuyer, error) {
	resultFindStoreByName, errFindStoreByName := bs.storeRepo.FindStoreByName(ctx, input.StoreName)
	if errFindStoreByName != nil {
		return nil, errFindStoreByName
	}

	resultFindBookByTitle, errFindBookByTitle := bs.bookRepo.FindBookByTitle(ctx, input.BookTitle)
	if errFindBookByTitle != nil {
		return nil, errFindBookByTitle
	}

	resultFindRelationshipByStoreAndBookId, errFindRelationshipByStoreAndBookId := bs.bookStoreRelationshipRepo.FindRelationshipByStoreAndBookId(ctx, resultFindStoreByName.Id.Hex(), resultFindBookByTitle.Id.Hex())
	if errFindRelationshipByStoreAndBookId != nil {
		return nil, errFindRelationshipByStoreAndBookId
	}

	resultFindBuyer, errFindBuyer := bs.userRepo.FindUserById(ctx, buyerId)
	if errFindBuyer != nil {
		return nil, errFindBuyer
	}

	if resultFindBuyer.Role != "Buyer" {
		return nil, errors.New("User tidak diizinkan!")
	}

	_, errFindRelationshipByBuyerAndBookId := bs.bookBuyerRelationshipRepo.FindRelationshipByBuyerAndBookId(ctx, buyerId, resultFindBookByTitle.Id.Hex())
	if errFindRelationshipByBuyerAndBookId == nil {
		return nil, errors.New("Buku sudah punya!")
	}

	resultFindMerchantByStoreId, errFindMerchantByStoreId := bs.merchantStoreRelationshipRepo.FindMerchantByStoreId(ctx, resultFindStoreByName.Id.Hex())
	if errFindMerchantByStoreId != nil {
		return nil, errFindMerchantByStoreId
	}

	resultFindMerchant, errFindMerchant := bs.userRepo.FindUserById(ctx, resultFindMerchantByStoreId.MerchantId.Hex())
	if errFindMerchant != nil {
		return nil, errFindMerchant
	}

	BuyerBalance := entity.UserTopUpBalanceInput{
		Amount: resultFindBuyer.Balance - resultFindBookByTitle.Price,
	}

	MerchantBalance := entity.UserTopUpBalanceInput{
		Amount: resultFindMerchant.Balance + resultFindBookByTitle.Price,
	}

	opts := options.Session().SetDefaultReadConcern(readconcern.Majority())
	sess, errStartSess := bs.client.StartSession(opts)
	if errStartSess != nil {
		return nil, errStartSess
	}
	defer sess.EndSession(ctx)

	errWithSess := mongo.WithSession(ctx, sess, func(sessCtx mongo.SessionContext) error {
		if errStartTransaction := sess.StartTransaction(); errStartTransaction != nil {
			return errStartTransaction
		}

		_, errCreateBookBuyerRelationship := bs.bookBuyerRelationshipRepo.CreateBookBuyerRelationship(sessCtx, resultFindBuyer.Id.Hex(), resultFindBookByTitle.Id.Hex())
		if errCreateBookBuyerRelationship != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errCreateBookBuyerRelationship
		}

		_, errUpdateBookStock := bs.bookStoreRelationshipRepo.UpdateBookStock(sessCtx, resultFindStoreByName.Id.Hex(), resultFindBookByTitle.Id.Hex(), resultFindRelationshipByStoreAndBookId.Stock - 1)
		if errUpdateBookStock != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errUpdateBookStock
		}

		_, errUpdateBuyerBalance := bs.userRepo.UpdateUserBalance(sessCtx, &BuyerBalance, resultFindBuyer.Id.Hex())
		if errUpdateBuyerBalance != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errUpdateBuyerBalance
		}

		_, errUpdateMerchantBalance := bs.userRepo.UpdateUserBalance(sessCtx, &MerchantBalance, resultFindMerchant.Id.Hex())
		if errUpdateMerchantBalance != nil {
			errRb := sess.AbortTransaction(ctx)
			if errRb != nil {
				return errRb
			}
			return errUpdateMerchantBalance
		}

		return sess.CommitTransaction(ctx)
	})
	if errWithSess != nil {
		return nil, errWithSess
	}
	
	resultsFindBookListByUserId, errFindBookListByUserId := bs.bookBuyerRelationshipRepo.FindBookListByBuyerId(ctx, resultFindBuyer.Id.Hex())
	if errFindBookListByUserId != nil {
		return nil, errFindBookListByUserId
	}

	bookList := make([]entity.BuyerBookData, 0)
	for _, book := range resultsFindBookListByUserId {
		resultFindBookById, errFindBookById := bs.bookRepo.FindBookById(ctx, book.BookId.Hex())
		if errFindBookById != nil {
			return nil, errFindBookById
		}

		bookData := entity.BuyerBookData{
			BookTitle: resultFindBookById.BookTitle,
			BookAuthor: resultFindBookById.BookAuthor,
			YearPublished: resultFindBookById.YearPublished,
		}

		bookList = append(bookList, bookData)
	}

	output := entity.BookListByBuyer{
		UserName: resultFindBuyer.UserName,
		Role: resultFindBuyer.Role,
		BookList: bookList,
	}

	return &output, nil
}

func (bs *BookService) FindBookListByBuyer(ctx context.Context, buyerId string) (*entity.BookListByBuyer, error) {
	resultFindUserById, errFindUserById := bs.userRepo.FindUserById(ctx, buyerId)
	if errFindUserById != nil {
		return nil, errFindUserById
	}

	if resultFindUserById.Role != "Buyer" {
		return nil, errors.New("User tidak diizinkan!")
	}

	resultsFindBookListByBuyerId, errFindBookListByBuyerId := bs.bookBuyerRelationshipRepo.FindBookListByBuyerId(ctx, buyerId)
	if errFindBookListByBuyerId != nil {
		return nil, errFindBookListByBuyerId
	}

	bookList := make([]entity.BuyerBookData, 0)
	for _, book := range resultsFindBookListByBuyerId {
		resultFindBookById, errFindBookById := bs.bookRepo.FindBookById(ctx, book.BookId.Hex())
		if errFindBookById != nil {
			return nil, errFindBookById
		}

		bookData := entity.BuyerBookData{
			BookTitle: resultFindBookById.BookTitle,
			BookAuthor: resultFindBookById.BookAuthor,
			YearPublished: resultFindBookById.YearPublished,
		}

		bookList = append(bookList, bookData)
	}

	output := entity.BookListByBuyer{
		UserName: resultFindUserById.UserName,
		Role: resultFindUserById.Role,
		BookList: bookList,
	}

	return &output, nil
}